const express = require('express');

const AuthController = require('../controllers/auth.controller');
const ProductController = require('../controllers/product.controller');

const router = express.Router();

const authController = new AuthController();
authController.register(router);

const productController = new ProductController();
productController.register(router);

module.exports = router;