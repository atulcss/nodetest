const mongoose = require("mongoose");
const productSchema = mongoose.Schema({
    product_name : {
        type : String,
        required:true
    },
    price : {
        type : Number,
    },
    category : {
        type : String,
    },
    brand : {
        type : String,
    },
    sales_count : {
        type : Number,
    },
    sale_date : {
        type : Date,
    },
});


module.exports = mongoose.model("Product",productSchema);