const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const userSchema = mongoose.Schema({
    name : {
        type : String,
    },
    email : {
        type : String,
        required : [true,"Email is required"],
        unique : true,
    },
    password : {
        type : String,
        required : [true,"Password is required"],
    },
    user_group : {
        type : String,
        required : [true,"User group is required"],
    },
});

userSchema.pre("save", async function(next){
    const salt = await bcrypt.genSalt();
    this.password = await bcrypt.hash(this.password,salt);
    next();
});

module.exports = mongoose.model("Users",userSchema);