module.exports = {
    userRegister: {
      id: "/userRegister",
      type: "object",
      required: ["email", "password","user_group"],
      properties: {
        name: { type: "string" },
        email: { type: "string" },
        password: { type: "string" },
        user_group: { type: "string" },
      },
    }
};