const User = require("../models/user-model");
const jwt = require("jsonwebtoken");

const auth = (req, res, next) => {
    try{
        let token = req.headers.authorization;
        if(token){
            let user = jwt.verify(token,"secret key");
            req.userId = user.id
        }else{
            console.log(token);
            req.status(401).json({message : "Unauthorised user"});
        }
        next();
    } catch(error) {
        
        res.status(401).json({message: "Unauthorised user"});
    }
    
}

module.exports = auth;
    