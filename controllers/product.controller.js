const Product = require("../models/product-model");
var bodyParser = require('body-parser');
var auth = require('../middlewares/auth.middleware');

class ProductController {

	register(app) {

        var jsonParser = bodyParser.json()

        app.route('/product')
			.post(jsonParser,auth,async (req, res, next) => {
				try {

                    console.log(req.body);
                    const product = await Product.create({...req.body});
                   
                    res.status(201).json({product:product._id,created: true});
				} catch (error) {
					next(error);
				}
			})
            
            .get(async (req, res, next) => {
				try {
                    let filter = req.query;
					let offset = filter.offset || 0;
                    let limit = filter.limit || 10;
                    const order_by = filter.orderBy || "product_name";
                    const sort_by = filter.sortBy || "ASC";
                    limit = parseInt(limit);
                    offset = parseInt(offset);
                    const product = await Product.find().sort({ [order_by]: sort_by }).skip(offset).limit(limit);
                    
                    res.status(201).json({product:product});
				} catch (error) {
					next(error);
				}
			})

            app.route('/top-ten-product')
			.get(jsonParser,auth,async (req, res, next) => {
				try {
					let offset =  0;
                    let limit = 10;
                    const order_by = "sales_count";
                    const sort_by = "DESC";
                    limit = parseInt(limit);
                    offset = parseInt(offset);
                    const product = await Product.find({
                        "sale_date": 
                        {
                            $gte: new Date((new Date().getTime() - (30 * 24 * 60 * 60 * 1000)))
                        }
                    }).sort({ [order_by]: sort_by }).skip(offset).limit(limit);
                    
                    res.status(201).json({product:product});
				} catch (error) {
					next(error);
				}
			})

            app.route('/product/:id')
                .put(jsonParser,auth,async (req, res, next) => {
                    try {
                        
                        const product = await Product.findOneAndUpdate({_id:req.params.id});
                    
                        res.status(201).json({product:product._id,created: true});
                    } catch (error) {
                        next(error);
                    }
                })
            .delete(async (req, res, next) => {
                try {
                        
                    const product = await Product.findOneAndRemove({_id:req.params.id}, req.body);
                
                    res.status(201).json({product:product._id,created: true});
                } catch (error) {
                    next(error);
                }
            })
    }

}

module.exports = ProductController;