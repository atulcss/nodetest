const User = require("../models/user-model");
var bodyParser = require('body-parser');
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");

class AuthController {

	register(app) {

        const maxAge = 3 * 24 * 60 * 60;

        var jsonParser = bodyParser.json()

        app.route('/register')
			.post(jsonParser,async (req, res, next) => {
				try {
					
                    console.log(req.body);
                    const user = await User.create({...req.body});
                    
                    const token = jwt.sign({id:user._id,email:user.email}, "secret key", {
                        expiresIn:maxAge,
                    });
                    res.status(201).json({user:user._id,created: true});
				} catch (error) {
					next(error);
				}
			});

            app.route('/login')
			.post(jsonParser,async (req, res, next) => {
				try {
					
                    console.log(req.body);
                    const {email,password} = req.body;
                    const user = await User.findOne({email : email});
                    
                    if(user){
                        const auth = await bcrypt.compare(password,user.password);
                        if(auth){
                           const token =  jwt.sign({id:user._id,email:user.email}, "secret key", {
                                expiresIn:maxAge,
                            });
                        
                            res.status(200).json({user:user._id,token:token,created: true});
                        }else{
                            throw Error("Incorrect password")
                        }
                    }else{
                        throw Error("Incorrect email")
                    }
				} catch (error) {
					next(error);
				}
			});
    }

}

module.exports = AuthController;